#include <iostream>
#include "cat.h"
using namespace std;

int main() {
  cout << "Meow world\n";
  Cat cat;
  cat.meow();
  cat.changeName("Cat Lala");
  cat.meow();
  cout << "-----\n";
  Cat cat2("John Cena");
  cat2.meow();
  return 0;
}
