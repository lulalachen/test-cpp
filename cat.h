using namespace std;

class Cat {
public:
  Cat ();
  Cat (const string newName);
  void meow ();
  void changeName (const string newName);
private:
  string name;
};
