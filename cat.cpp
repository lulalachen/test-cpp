#include <iostream>
#include "cat.h"
using namespace std;

Cat::Cat() {
  name = "Cat Cena";
}

Cat::Cat(const string newName) {
  name = newName;
}

void Cat::changeName(const string newName) {
  name = newName;
}

void Cat::meow() {
  cout << "Meow I'm " + name + "\n";
}
